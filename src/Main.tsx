import React, { useState } from 'react'

const Main: React.FC = () => {
    const data = localStorage.getItem('POINT')
    const [point, setPoint] = useState(parseInt(data|| '') ?? 0)

    const AddPoint = () => {
        localStorage.setItem('POINT', `${point + 1}`)
        setPoint(point + 1)
    }

    const Reset = () => {
        localStorage.setItem('POINT', '0')
        setPoint(0)
    }

    return (
            <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                width: window.innerWidth,
                height: window.innerHeight,
                alignItems: 'center',
                justifyContent: 'center',
                background: '#8AB6D6'
            }}>
            <text
                style={{
                    padding: '56px',
                    fontSize: '24px'
                }}
            >
                {`Tu as fait ${point} point${point > 1 ? 's' : ''}`}
            </text>
            <div style={{display: 'flex', flexDirection:'row'}}>
                <div
                style={{
                    display: 'flex',
                    padding: 8,
                    background: '#FBE0C4',
                    borderRadius: '8px',
                        margin: '4px',
                    
                }}
                    onClick={AddPoint}>
                    Ajouter
                </div>
                <div
                style={{
                    display: 'flex',
                    padding: 8,
                    background: '#0061A8',
                    borderRadius: '8px',
                    margin: '4px',
                    color: '#8AB6D6'
                }}
                    onClick={Reset}
                >
                    Réinitialiser
                </div>
                </div>
            </div>
    )
}

export default Main